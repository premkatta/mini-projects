import React from 'react'
import '../css/linechart.css'
import { LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, Legend,
	BarChart, Bar
} from 'recharts';
const data = [{name: 'Page A', uv: 400, pv: 2400, amt: 2400}]

class ChartComp extends React.Component{
	constructor(props){
		super(props)
		this.state={}
	}

	render(){
		return(
			<div>
			<div className='linechart-div'>
			<LineChart width={730} height={250} data={data} style={{display:'inline-block'}} className='linecharts'
			  margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
			  <CartesianGrid strokeDasharray="3 3" />
			  <XAxis dataKey="name" />
			  <YAxis />
			  <Tooltip style={{visibility:'hidden'}} />
			  <Legend />
			  <Line type="monotone" dataKey="pv" stroke="#8884d8" />
			  <Line type="monotone" dataKey="uv" stroke="#82ca9d" />
		</LineChart>
		</div>
			<BarChart width={730} height={250} data={data} style={{display:'inline-block'}} >
			<CartesianGrid strokeDasharray="3 3" />
			<XAxis dataKey="name" />
			<YAxis />
			<Tooltip />
			<Legend />
			<Bar dataKey="pv" fill="#8884d8" />
			<Bar dataKey="uv" fill="#82ca9d" />
		</BarChart>
		</div>
		)
	}

}

export default ChartComp 
