import React from 'react'
import '../css/App.css'
import ChartComp from './LineChart'

class App extends React.Component{
	constructor(props){
		super(props)
		this.state={
			username:'',
			email: '', 
			totalData:[],
			act:0
		}
	}

	usernameChange=(e)=>{
		this.setState({
			username: e.target.value
		})
	}
	
	emailChange=(e)=>{
		this.setState({
			email: e.target.value
		})
	}

	formSubmit=(e)=>{
		e.preventDefault()
		let username = this.state.username
		let email = this.state.email
		let totalData = this.state.totalData
		let data={
			username,
			email
		}

		if(this.state.act === 0){
			totalData.push(data)
			this.setState({totalData})
		} else{
			let index = this.state.index
			totalData[index].username = username
			totalData[index].email = email
		}

		this.setState({
			act:0,
			totalData
		})
		
		this.refs.myForm.reset()
		this.refs.username.focus()
	}


	delete=(i)=>{
		this.state.totalData.splice(i, 1)
		let totalData = this.state.totalData
		this.setState({totalData})
		this.refs.username.focus()
	}
	
	edit=(i)=>{
		console.log(this.state.totalData[i])
		let data= this.state.totalData[i]
		this.refs.username.value = data.username
		this.refs.email.value = data.email
		this.refs.username.focus()

		this.setState({
			act : 1,
			index : i
		})

	}

	renderedUserData=()=>{

		return this.state.totalData.map((data,i)=>{
			return(
				<div key={i} className='user-details-row'>
					<span>{data.username}</span>
					<span >{data.email}</span>
					<span onClick={()=>this.edit(i)}>Edit</span>
					<span className='delete-data' onClick={()=>this.delete(i)}>Delete</span>
				</div>
			)
		})
	}

	render(){
		return(
			<div>
				<div className='main-div'>
					<form ref='myForm' onSubmit={this.formSubmit}>
					<div className='username-div'>
						<label>
							username
						</label>
						<input ref='username' type='text' placeholder='Enter username' onChange={(e)=>{this.usernameChange(e)}}/>
					</div>
					<div className='email-div'>
						<label>
							email 1
						</label>
						<input type='text' ref='email' placeholder='Enter email' onChange={(e)=>{this.emailChange(e)}} />
					</div>
					<button>Submit</button>
					</form>
				</div>
				<div className='display-div'>
					{this.renderedUserData()}
				</div>
				<div className='linechart-app-div'>
					<ChartComp />
				</div>
			</div>
		)
	}
}

export default App