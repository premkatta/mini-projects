import React from 'react';
import Home from './Home';
import '../css/App.css';

class App extends React.Component{
  render(){
    return(
      <div>
        <Home />
      </div>
    );
  }
}

export default App;