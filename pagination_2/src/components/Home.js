import React from 'react';
import Welovedaily from '../apis/Welovedaily';
import '../css/Home.css';

class Home extends React.Component{

  constructor(){
    super();
    this.state={illustration:[], pageData:[], totalPages:null, itemsPerPage:20, currentPage:1, results:[1,20]};
  }
  
  componentDidMount=()=>{
    Welovedaily.get('photos').then(response=>{
			console.log(response);
      this.setState({illustration: response.data})
      const pageData = this.state.illustration.slice((this.state.currentPage-1) * this.state.itemsPerPage, (this.state.currentPage) * this.state.itemsPerPage );
      const totalPages = Math.ceil(this.state.illustration.length / this.state.itemsPerPage) ;
      this.setState({pageData, totalPages});
      this.enterCalculatePages();
    })
  }

  
  enterCalculatePages = ()=> {
    const totalPages = Math.ceil(this.state.illustration.length / this.state.itemsPerPage) ;
    const pageData = this.state.illustration.slice((this.state.currentPage-1) * this.state.itemsPerPage, (this.state.currentPage) * this.state.itemsPerPage );
    let results = [((this.state.currentPage -1) * this.state.itemsPerPage)+1, (this.state.currentPage) * this.state.itemsPerPage];
    this.setState({totalPages, pageData, results});
  }

  pageClick(pageNo){
    this.setState({currentPage:pageNo});
    const pageData = this.state.illustration.slice((pageNo-1) * this.state.itemsPerPage, pageNo * this.state.itemsPerPage );
    let results = [((pageNo-1)*this.state.itemsPerPage)+1, pageNo * this.state.itemsPerPage];
    this.setState({pageData, results});
  }
  
  totalPages(){
    let arr=[];
    for(let i=1; i<=this.state.totalPages; i++){
      arr.push(
        <div key={i} className={`pageNumber ${(this.state.currentPage === i) || (this.state.currentPage === 0) ? 'activePage' : ''}`} onClick={()=>this.pageClick(i)}>{i}</div>
      );
    }
    return arr;
  }

   prev=()=>{
     const prev = this.state.currentPage - 1;
     this.setState({currentPage:prev});
     console.log(this.state.currentPage - 1);
     const pageData = this.state.illustration.slice((this.state.currentPage -2 ) * this.state.itemsPerPage, (this.state.currentPage-1) * this.state.itemsPerPage );
     let results = [((this.state.currentPage -2 ) * this.state.itemsPerPage)+1, (this.state.currentPage-1) * this.state.itemsPerPage];
     this.setState({ pageData, results});
     console.log(this.state);
   }

   next=()=>{
    const next = this.state.currentPage + 1;
    this.setState({currentPage:next});
    console.log(next,this.state.currentPage + 1);
    const pageData = this.state.illustration.slice((next * this.state.itemsPerPage ), (next +1) * this.state.itemsPerPage );
    let results = [((this.state.currentPage -2 ) * this.state.itemsPerPage)+1, (this.state.currentPage-1) * this.state.itemsPerPage];
    this.setState({ pageData, results});
    console.log(this.state);
   }

  renderedImage(){
    return this.state.pageData.map(obj=>{
			return <div style={{display:'inline-block',padding:'15px'}}>{obj.id}</div>
      // return <img className='image' src={obj.url} alt={obj.title} />
    });
  }

  render(){
		// debugger
    return(
      <div>
        <div className='header'>
          <a href='#abc' className='logo'>Logo</a>
          <div className='menuDiv'>
            <a href='#abc'>Menu1</a>
            <a href='#abc'>Menu2</a>
          </div>
        </div>
        <div className='inputDiv'>
          <input type='number' placeholder='Enter number' onChange={(event)=>this.setState({itemsPerPage:Number(event.target.value)})}/>
          {this.state.itemsPerPage}
          <button onClick={this.enterCalculatePages}>Search</button>
        </div>
        <div className='results'>
          Displaying {this.state.results[0]<1 ? '0' : this.state.results[0]} to {this.state.results[1]>this.state.illustration.length ? this.state.illustration.length : this.state.results[1] }
        </div>
        {this.renderedImage()}
        <div>
          <a onClick={this.prev}>prev</a>
            {this.totalPages()}
          <a onClick={this.next}>next</a>
        </div>
      </div>
    );
  }
}

export default Home;