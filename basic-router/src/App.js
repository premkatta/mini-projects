import React, { Component } from 'react';
import {BrowserRouter, Route, Switch } from "react-router-dom";
import Home from "./components/Home";
import About from "./components/About";
import Error from "./components/Error";
import Navigation from "./components/Navigations";

class App extends Component {
  render() {
    return (
    <BrowserRouter>
      <div>
        <Navigation>  </Navigation>
        <Switch>
          <Route path = "/"  exact component = {Home}/>
          <Route path = "/about" component = {About}/>
          <Route component = {Error}/>
        </Switch>
      </div>
    </BrowserRouter>
    );
  }
}

export default App;
