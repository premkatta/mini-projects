import React from "react";
const Footer = ()=>{
    return(
        <div className = "footer">
        <div className = "footer-container">
            <div className = "column">
                <h4>welovedaily</h4>
                <li> <a className = "click"> Inspiration </a></li>
                <li> <a className = "click"> challenges </a></li>
                <li> <a className = "click"> Advertise </a></li>
                <li> <a className = "click"> About us</a></li>
                <li> <a className = "click"> submit </a></li>
            </div>
            <div className = "column">
                <h4>Docs.</h4>
                <li> <a className = "click"> Submitting work </a></li>
                <li> <a className = "click"> Joining a challenge </a></li>
                <li> <a className = "click"> Hosting a challenge </a></li>
            </div>
            <div className = "column">
                    <h4>Instagram</h4>
                    <li> <a className = "click"> Web Design <label class = "countWd">283K</label> </a></li>
                    <li> <a className = "click"> Branding <label class = "countBr" >247K</label>  </a></li>
                    <li> <a className = "click"> Illustration <label class = "countIl" >99.2K</label>  </a></li>
                    <li> <a className = "click"> Animations <label  class = "countAn">86.9K</label> </a></li>
            </div>
            <div className = "column">
                    <h4>Get in touch</h4>
                    <li> <a className = "click"> hello@welovedaily.com </a></li>
                    <div className = "social-links-about">
                        <a href="https://www.facebook.com/welovedaily" className="share-button facebook" target="_blank"></a>
                        <a href="https://twitter.com/welovedaily" className="share-button twitter" target="_blank"></a>
                        <a href="https://linkedin.com/welovedaily" className="share-button linkedIn" target="_blank"></a>
                    </div>
            </div>

        </div>
        </div>
    )
};

export default Footer;