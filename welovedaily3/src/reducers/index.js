import {combineReducers} from 'redux';
import animationDatapp from './animation';
import webDesignData from './webDesign';
import illustrationData from './illustration';
import brandingData from './branding';
import homeData from './home';

let rootReducer = combineReducers({
    anim: animationDatapp,
    webDes : webDesignData,
    illus : illustrationData,
    brand : brandingData,
    hom: homeData
})

export default rootReducer;
