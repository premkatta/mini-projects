export default function (state = [], action) {
    switch (action.type) {
        case "BRANDING_DATA":
            console.log("reducer", action.data);
            return action.data;
        case "NO_DATA_LOADED":
            return action.data;
    }
    return state;
}
