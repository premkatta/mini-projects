import React, { Component } from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import {NavLink} from "react-router-dom";
// import logo from './logo.svg';
import '../css/App.css';

import { returnData } from '../actions/index';
import homes from "../components/home";
import webDesign from "../components/webDesign";
import animation from "../components/animation";
import illustration from "../components/illustration";
import branding from "../components/branding";
import Footer from "../reducers/footer";


class App extends Component {


  componentDidMount() {
    // console.log(this.props.animation);
    this.props.returnData();
  }


  render() {
    return (
      <BrowserRouter>
      
      <div>
     
      <div className="App">
        <div className="leftPanel">
          <div className="menuIcon"></div>
        </div>
        <div className="rightpanel">
          <div className="divSubmit">
            Submit your work
          </div>
          <div className="divBody">
          <NavLink to = "/"> <div className="mainIcon"> </div></NavLink> 
            <div className="labelInspiration">
              Inspiration
            </div>
            <h2 className="mainHeading">Daily inspiration, right in your face.</h2>
            <div className = "tags"> Tags:
              <span className = "blocks">
              <div className = "tag"> <NavLink to = "/webDesign">WebDesign</NavLink></div>
              <div className = "tag"> <NavLink to = "/branding">Branding</NavLink> </div>
              <div className = "tag"> <NavLink to = "/illustration">Illustration</NavLink> </div>
              <div className = "tag"> <NavLink to = "/animation">animation</NavLink> </div>
              </span>
                <Route path = "/"  exact component = {homes}> </Route>
                <Route path = "/webDesign"  exact component = {webDesign}> </Route>
                <Route path = "/branding" exact component = {branding}></Route>
                <Route path = "/illustration" exact component = {illustration}></Route>
                <Route path = "/animation" exact component = {animation}></Route>
                {/* <div className = "tag" >Branding</div>
                <div className = "tag">Illustration</div>
                <div className = "tag">Animation</div>  */}
             
             
            </div>
           </div>
           <div>
          </div>
        </div>
       
      </div>
      <Footer></Footer>
      </div>
      </BrowserRouter>
      
      
    );
  }
}

function mapStateToProps(state) {
  // console.log("state",state);
  return { animation: state.anim };
}

function mapDisptachToProps(dispatch) {
  return bindActionCreators({ returnData }, dispatch);
}

export default connect(mapStateToProps, mapDisptachToProps)(App);

