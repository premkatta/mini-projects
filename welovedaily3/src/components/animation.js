import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';

function mapStateToProps(state) {
    console.log("state",state);
    console.log("state anim",state.anim);
    return { animation: state.anim };
    
  }

class animation extends Component{
    constructor(props){
        super()
    }

render(){
    return(
    <div> 
   {
       
      
            this.props.animation.map((e,index)=>{
                if(index<=10){
                // console.log(e.img);
                return  <div className = "post">
                <div className = "post-container">
                <img class = "card-img-top" src = {e.img}></img>
                </div>
                <div className= "post-Description">
                by <label className = "des-click">{e.artist}</label> 
                <label class = "des-right">{e.linkType}</label>
                </div>
              </div>
              
              
                }
            })
        
        
   }
   </div>
    )

};

}

export default connect(mapStateToProps) (animation);