import React from 'react';
import JsonApp from '../apis/JsonApp';
import '../css/Homepage.css';

class Homepage extends React.Component{
	
	constructor(props){
		super(props);
		this.state={totalData:[],displayCount:50,totalPages:100,currentPage:1,displayData:[]};
	}

	componentDidMount(){
		JsonApp.get('photos').then(response=>{
			this.setState({totalData : response.data});
			this.setState({totalPages : Math.ceil(this.state.totalData.length / this.state.displayCount)});
			let displayData = this.state.totalData.slice((this.state.currentPage - 1) * this.state.displayCount, this.state.currentPage * this.state.displayCount); 
			this.setState({displayData});
			console.log(this.state.totalData, this.state.totalPages);
		});
	}

	// For referrence async await
	// async componentDidMount() {
  //   const response = await fetch(`https://api.coinmarketcap.com/v1/ticker/?limit=10`);
  //   const json = await response.json();
  //   this.setState({ data: json });
  // }

	search=(event)=>{
		event.preventDefault();
		this.setState({totalPages : Math.ceil(this.state.totalData.length / this.state.displayCount)});
		let displayData = this.state.totalData.slice((this.state.currentPage - 1) * this.state.displayCount, this.state.currentPage * this.state.displayCount); 
		this.setState({displayData});
		setTimeout(()=>{console.log(this.state, displayData)},1000);
	}
	
	renderedData(){
		return this.state.displayData.map(data=>{
			// return <a style={{display:'inline-block',padding:'2px'}}>{data.id}</a>
			return <img style={{display:'inline-block',padding:'2px', width:'55px'}} src={data.url}/>
		})
	}

	pageClick(i){
		this.setState({currentPage:i});
		setTimeout(()=>{console.log(this.state)},1000);
		let displayData = this.state.totalData.slice((i - 1) * this.state.displayCount, i * this.state.displayCount); 
		this.setState({displayData});
	}
	
	prev(){
		console.log('prev clicked');
		let prevPage = this.state.currentPage - 1; 
		this.setState({currentPage:prevPage});
		setTimeout(()=>{console.log(this.state)},1000);
		let displayData = this.state.totalData.slice((prevPage - 1) * this.state.displayCount, prevPage * this.state.displayCount); 
		this.setState({displayData});
	}
	
	next(){
		let nextPage = this.state.currentPage + 1; 
		this.setState({currentPage:nextPage});
		setTimeout(()=>{console.log(this.state)},1000);
		let displayData = this.state.totalData.slice((nextPage - 1) * this.state.displayCount, nextPage * this.state.displayCount); 
		this.setState({displayData});
	}

	totalPages(){
		let pagesArray = [];
		for(let i=1;i<=this.state.totalPages;i++){
			pagesArray.push(
				<div className={`pageNumber ${this.state.currentPage === i || this.state.currentPage === 0 ? 'activePage' : 'null'}`} key={i} onClick={()=>this.pageClick(i)}>{i}</div>
			);
		}
		return pagesArray;
	}

	render(){
		// debugger
		return(
			<div className='homepage-main'>
				<div className='navbar-main'>
					<div className='logo'> Logo </div>
					<div className='nav-right'>
						<span>Settings</span>
						<span>Dashboard</span>
					</div>
				</div>
				<div className='count-main'>
					{/* Displaying {this.state.} */}
				</div>
				<div className='input-main'>
					<form onSubmit={this.search}>
						<input type='number' placeholder='enter number' onChange={e=>{this.setState({displayCount:Number(e.target.value)})}}/>
						{this.state.displayCount}
						<button type='submit'>Submit</button>
					</form>
				</div>
				<div className='data-display-main'>
					{this.renderedData()}
				</div>
				<div className='total-pages'>
					{/* Here clicking on prev page (if currentPage = 1 then prev should not happen and prev should disable) */}
					<a onClick={this.state.currentPage>=1 ? (()=>{this.prev()} ) : alert('last page')}>prev</a>
						{this.totalPages()}
					<a onClick={()=>{this.next()}}>next</a>
				</div>
			</div>
		);
	}
}

export default Homepage;