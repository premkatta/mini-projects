import React from 'react';
import './App.css';
import axios from "axios";

class App extends React.Component {

  constructor() {
    super();
    this.state = { animation: [], itemsPerPage: 20, pageData: [], currentPage: 0, totalPages: 0 };
  }

  componentDidMount() {
    axios.get('https://jsonplaceholder.typicode.com/photos').then(res => {
      this.setState({ animation: res.data });
      let pageData = res.data.slice(this.state.currentPage * this.state.itemsPerPage, (this.state.currentPage + 1) * this.state.itemsPerPage);
      let totalPages = Math.ceil(res.data.length / this.state.itemsPerPage)
      this.setState({ pageData, totalPages })
    })
  }

  pageClick(pageNo) {
    let pageData = this.state.animation.slice(pageNo * this.state.itemsPerPage, (pageNo + 1) * this.state.itemsPerPage);
    this.setState({ pageData, currentPage: pageNo });
  }

  calculatePages() {
    let totalPages = Math.ceil(this.state.animation.length / this.state.itemsPerPage)
    let pageData = this.state.animation.slice(this.state.currentPage * this.state.itemsPerPage, (this.state.currentPage + 1) * this.state.itemsPerPage);
    this.setState({ totalPages, pageData });
  }

  returnPages() {
    let arr = [];
    for (let i = 0; i < this.state.totalPages; i++) {
      arr.push(<div className={`page ${this.state.currentPage == i ? 'activePage' : ''}`} onClick={() => { this.pageClick(i) }}>{i + 1}</div>);
    }
    return arr;
  }

  render() {
    return <div>
      <div className="divNavBar">
        <div className="divLogo">MyLogo</div>
        <div className="divRightNav">
          <div className="navItem">About</div>
          <div className="navItem">Kill</div>
        </div>
      </div>
      <input type="number" placeholder="Items per page" value={this.state.itemsPerPage} onChange={(ev) => { this.setState({ itemsPerPage: ev.target.value }) }} />
      <label>{this.state.itemsPerPage}</label>
      <button onClick={() => { this.calculatePages() }}>Enter</button>
      <div className="divImages">
        {
          this.state.pageData.map((e) => {
          return <img src={e.url} />
          })
        }
      </div>
      {
        this.returnPages()
      }
    </div>
  }

}

export default App;