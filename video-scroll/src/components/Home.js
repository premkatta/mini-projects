import React from "react"
import Youtube from "../apis/Youtube"
import "../css/Home.css"

class Home extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      searchTerm: "beauty of karimnagar",
      totalVideos: [],
      pageData: [],
      selectedVideo: "",
      totalItems: 11,
      currentPage: 0,
      videoSrc: "",
    }
  }

  componentDidMount() {
    this.search()
  }

  search = (event) => {
    console.log("enterign")
    if (event) {
      event.preventDefault()
    }
    // console.log(this.state.searchTerm);
    Youtube.get("", {
      params: {
        part: "snippet",
        maxResults: this.state.totalItems,
        key: "AIzaSyBc62BlrCa8hXyzlMUWh5EWNAzpT2JLcp4",
        q: this.state.searchTerm,
        // id: 12345,
      },
    })
      .then((response) => {
        this.setState({ totalVideos: response.data.items })
        console.log("totalVideos", response.data.items)
      })
      .then((a) => {
        console.log("testing", a)
        let pageData = this.state.totalVideos.slice(
          this.state.currentPage * 11,
          (this.state.currentPage + 1) * 11
        )
        this.setState({
          pageData,
          selectedVideo: pageData[0].snippet.thumbnails.high.url,
          videoSrc: `https://www.youtube.com/embed/${pageData[0].id.videoId}`,
        })
      })
      .catch((err) => {
        console.log(err, "error")
      })
  }

  videoList() {
    if (this.state.pageData.length === 0) {
      return <div className='loading'>Results will display here</div>
    } else {
      return this.state.pageData.map((video) => {
        return (
          <img
            className='image'
            key={video.etag}
            onClick={() => {
              this.selectedVideo(video)
            }}
            src={video.snippet.thumbnails.high.url}
            alt='alt'
          />
        )
      })
    }
  }

  selectedVideo = (video) => {
    this.setState({
      selectedVideo: video.snippet.thumbnails.high.url,
      videoSrc: `https://www.youtube.com/embed/${video.id.videoId}`,
    })
  }

  itemSet = () => {
    let pageData = this.state.totalVideos.slice(
      this.state.currentPage * this.state.totalItems,
      (this.state.currentPage + 1) * this.state.totalItems
    )
    this.setState({ pageData })
  }

  pageNumbers = () => {
    const arr = []
    this.state.totalItems =
      this.state.totalItems > 50 ? this.state.totalItems === 50 : this.state.totalItems
    const totalPages = Math.ceil(this.state.totalItems / 11)
    for (let i = 1; i <= totalPages; i++) {
      arr.push(
        <span
          className={`pageNumber ${this.state.currentPage === i ? "activePage" : ""}`}
          key={i}
          onClick={() => this.pageClick(i)}
        >
          {i}
        </span>
      )
    }
    return arr
  }

  pageClick = (i) => {
    this.setState({ currentPage: i })
    // console.log(i, this.state.currentPage);

    let pageData = this.state.totalVideos.slice((i - 1) * 11, i * 11)
    const videoSrc = `https://www.youtube.com/embed/${pageData[0].id.videoId}`
    this.setState({ pageData, videoSrc })
  }

  render() {
    return (
      <div>
        <div className='searchBox'>
          <form onSubmit={this.search}>
            <input
              className='searchInput'
              type='text'
              placeholder='search here'
              onChange={(event) => this.setState({ searchTerm: event.target.value })}
            />
            <input
              className='numberInput'
              type='number'
              placeholder='number'
              min='1'
              max='50'
              onChange={(event) => {
                this.setState({ totalItems: event.target.value })
              }}
            />
            <button className='submitButton' type='submit'>
              {" "}
              submit{" "}
            </button>
          </form>
        </div>
        <div className='videoDetailMain'>
          <div className='videoPlayerMain'>
            <iframe
              className='iframeVideo'
              title='videoPlay'
              src={this.state.videoSrc}
              frameBorder='0'
              allowFullScreen
              allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture'
            />
            {/* <img src={this.state.selectedVideo} alt='Vido will appear here' /> */}
          </div>
        </div>
        <div className='videoList'>
          <p>More Videos</p>
          <span>{this.videoList()}</span>
        </div>
        <div className='pageNumberDiv'>{this.pageNumbers()}</div>
      </div>
    )
  }
}

export default Home
