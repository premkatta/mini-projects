import React, {Component} from 'react';
import './contactForm.css';
import axios from 'axios';

class ContactForm extends Component{

    handleSubmit(e){
        e.preventDefault();
        const email=document.getElementById('email').value;
        const textarea= document.getElementById('textArea').value;
        
        alert(textarea);
        axios({
            method:'POST',
            url:'http://localhost:3000/send',
            data:{
                email,
                textarea
            }
        }).then((res)=>{
            if(res.data.msg==='success'){
                console.log(res.data);
                alert('Message submitted');
                this.resetForm();
            }else if(res.data.msg==='fail'){
                alert('Message not submitted')
            }
        }) 
    }

    resetForm(){
        document.getElementById('contact-form').reset();
    }

    render(){
        return(
            <div className="form-mainPanel">
            <form id="contact-form" onSubmit={this.handleSubmit.bind(this)} method ="POST">
                <h6>Email:</h6>
                    <input type="email" id="email" placeholder="placeholder"></input>
                <h6>Message:</h6>
                    <textarea id="textArea" ></textarea>
                <div><button type="submit">submit</button></div>
            </form>
            </div>
        )
    }
};


export default ContactForm;