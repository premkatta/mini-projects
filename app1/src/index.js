import React , {Component} from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Test from './Test';
import App3 from './Material-ui_testing';
import BitlabWeb from './BitlabWeb';
import Equal from './Equal';
// import Stumped from './stumped';
import Booktv from './Booktv';
// import registerServiceWorker from './registerServiceWorker';



let  NewApp = function(props){
    return(<div>
        <b>Hello, i'm {props.abc}</b>
    </div>);
}// Functional component
// in functional components can be accessed like props.name
// Functional components doesn't have any state
// Only class based components can use state


class SecondApp extends  React.Component {
    constructor(){
        super(); //
        this.state = { role : "software Developer" , bcd : "asa" , asasa :" aaaa" ,counter:0};
        // You can initialise state like this only in constructor function.
        // This.prop.xyz = 'asa'; //This is wrong. YOu cant change props in a child component.
        console.log('Initialization');
    }
    componentWillMount(){
        console.log('componentWillMount');
    }
    componentDidMount(){
        console.log('componentDidMount');
    }
    counterFunction(type){
        if(type == 'increase'){
            this.setState({
                counter:this.state.counter+1
            });
        }else{
            this.setState({
                counter:this.state.counter -1
            });
        }
    }
    render(){
        console.log('Render');
        return(
            <div>
                <NewApp abc ='Functional' />{/*this is how you give props to a child component*/}
                <em>Hello There, i'm a {this.props.xyz} component </em>
                {/* This is how you access props in a child based component*/}
                <h3> I'm a {this.state.role}</h3>
                {/* This is how you access state in a child based component*/}
                {/* <div className = 'abcClass' style={{backgroundColor:'green'}}onclick={() => this.setState({</div> */}
                <div className='abcClass' style={{backgroundColor :'green'}} onClick= {() => this.setState({role:'Full Stack Developer '})}> Change Role</div>
                <h2>{this.state.counter}</h2>
                <div className = 'btnChange' onClick ={()=>this.counterFunction("increase")}>Increase</div>
                <div className = 'btnChange' onClick ={()=>this.counterFunction("decrease")}>Decrease</div>
            </div>
        )

    }

}
// ReactDOM.render(<App3 />, document.getElementById('root'));
// ReactDOM.render(<BitlabWeb />,document.getElementById ('root'));
// ReactDOM.render(<Equal />,document.getElementById ('root'));

// ReactDOM.render(<task8 />, document.getElementById('root'));
// ReactDOM.render(<Stumped/>, document.getElementById('root'));
ReactDOM.render(<Booktv/>,document.getElementById('root'));

// ReactDOM.render(<Material-ui_testing/>,document.getElementById('root'));
// ReactDOM.render(<Test/>, document.getElementById('root'));


// ReactDOM.render(<SecondApp xyz= 'class based'/>,document.getElementById('root'));
// ReactDOM.render(<SecondApp/>,document.getElementById('root'));
// registerServiceWorker();
