import React, { Component } from 'react';
import axios from 'axios';
// import './App.css';
// import NewComponent from './NewComp';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      abc: "mona", abValue: "", mm: 56, superHeroes: [{
        key: 1,
        name: "Batman"
      }, {
        key: 2,
        name: "Spider-Man"
      }, {
        key: 3,
        name: "Daredevil"
      }, {
        key: 4,
        name: "Flash"
      }, {
        key: 5,
        name: "Iron Man"
      }], movieData: []
    }

    // this.changeABC = this.changeABC.bind(this);
  }

  changeABC() {
    this.setState({ abc: "Rapunzel" });
  }

  changeABCVAlue(ev) {
    console.log(ev);
    this.setState({ abValue: ev.target.value });
  }

  componentDidMount() {
    axios.get('https://stumped.herokuapp.com/api/posts').then((data) => {
      this.setState({ movieData: data });
    })
  }

  render() {
    return (
      <div className="App">
        {/* <NewComponent name={this.state.abc} /> */}
        <div>{this.state.abValue}</div>
        <input type="text" className="inpBox" value={this.state.abValue} onChange={(ev) => this.changeABCVAlue(ev)} />
        <button onClick={() => this.changeABC()}>Click Me!!!</button>

        {
          this.state.movieData.map((e) => {
            return <div className='divSupers' key={e.id}>{e.mainImage}</div>
          })
        }
      </div>
    );
  }
}

export default App;
