import React ,{Component} from 'react';
import axios from 'axios';
import  './Booktv.css' ;

class Booktv extends Component{
    constructor(props){
        super(props);
        this.state = {movieData : []}
    }

    // componentDidMount() {

    //     axios.get('localhost:3000/mongo').then((data) => {
    //       this.setState({ abc: data });
    //     })
    //   }

componentDidMount(){
    axios.get('https://api.themoviedb.org/3/movie/popular?api_key=de95a317ffd5c7d771e43cf0fb7c1bc5&language=en-US&page=1').then((data) =>{
        this.setState({movieData : data.data.results});
        
            console.log(this.state.movieData)
             
    })
}

render(){
    return(
        <div className = "total cards">
        {/* {
          this.state.movieData.map((e) => {
            return <div className='divSupers' key={e.id}>{e.title}</div>
          })
        } */}
        {
        this.state.movieData.map((e , index)=>{
            // console.log(index);
            // for (let i=0 ;i<10 ; i++){
                if(index %2 == 0){
                    console.log(index);
        return <div class="singleCard">
            <div className = "line"> </div>
            {/* <div>
            <img  src={'https://image.tmdb.org/t/p/w185_and_h278_bestv2'+e.poster_path }></img>
            <div className = "line"> </div>
              <div className = "info">
                
               <div className="titleHover titleFont"> {e.title}</div>
                <br></br>

                {e.vote_average}%
                <br></br>
                {e.release_date}
                <br></br>
                <br></br>
               
              </div>
            </div>
        </div> */}
             <div class="row ">
             <div class="col-lg-5 col-sm-2 col-4">
                    
                    </div>
                   
                    <div class=" imgIcons col-lg-2 col-sm-6 col-4">
                    <img className = "imgIcons" src={'https://image.tmdb.org/t/p/w185_and_h278_bestv2'+e.poster_path }></img>
                    </div>
                    <div class="col-lg-5 col-sm-2 col-4">
                    <div className = "info">
                
                <div className="titleHover titleFont"> {e.title}</div>
                 
                 
                 {e.vote_average}%
                 <br></br>
                 {e.release_date}
                 {e.overview}
                 <br></br>
                 <br></br>
                
               </div>
                    </div>
                   
                </div>
        </div>
            }
            else{
                return <div class="singleCard">
            
            {/* <div class= "cardColor">
            <div className = "line"> </div>
              <div className = "info">
                
               <div className="titleHover titleFont"> {e.title}</div>
                <br></br>
                
                {e.vote_average}%
                <br></br>
                {e.release_date}
                <br></br>
                <br></br>
               
              </div>
              <img className = "imgIcons" src={'https://image.tmdb.org/t/p/w185_and_h278_bestv2'+e.poster_path }></img>

            </div> */}
             <div className = "line"> </div>
                <div class="row cardColor">
                    <div class="col-lg-5 col-sm-2 col-4">
                    <div className = "info">
                
                <div className="titleHover titleFont"> {e.title}</div>
                 
                 
                 {e.vote_average}%
                 <br></br>
                 {e.release_date}
                 {e.overview}
                 <br></br>
                 <br></br>
                
               </div>
                    </div>
                    <div class=" imgIcons col-lg-2 col-sm-6 col-4">
                    <img className = "imgIcons" src={'https://image.tmdb.org/t/p/w185_and_h278_bestv2'+e.poster_path }></img>
                    </div>
                    <div class="col-lg-5 col-sm-2 col-4">
                    
                    </div>
                </div>
        </div>

            }
            })
        }
        </div>
    );
    

}
}
export default Booktv;