import React, { Component } from 'react';
import axios from 'axios';
// import logo from './logo.svg';
import './App.css';
// import NewComponent from './NewComp';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      abc: "mona", abValue: "", mm: 56, superHeroes: [{
        key: 1,
        name: "Batman"
      }, {
        key: 2,
        name: "Spider-Man"
      }, {
        key: 3,
        name: "Daredevil"
      }, {
        key: 4,
        name: "Flash"
      }, {
        key: 5,
        name: "Iron Man"
      }], movieData: []
    }

    // this.changeABC = this.changeABC.bind(this);
  }

  changeABC() {
    this.setState({ abc: "Rapunzel" });
  }

  changeABCVAlue(ev) {
    console.log(ev);
    this.setState({ abValue: ev.target.value });
  }
  // let obj  = {a:2,b:3};
  // let obj2 = {a:2,b:3};
  // obj == obj2 //false //because objects are compared by refference
  // obj === obj2 // false




  componentDidMount() {
    // https://api.themoviedb.org/3/movie/popular?api_key=99c26306be3d1ce05090f2450ff90c6e&language=en-US&page=1
    axios.get('localhost:3000/mongo').then((data) => {
      this.setState({ movieData: data });
    })
  }

  render() {
    return (
      <div className="App">
        {/* <NewComponent name={this.state.abc} /> */}
        <div>{this.state.abValue}</div>
        <input type="text" className="inpBox" value={this.state.abValue} onChange={(ev) => this.changeABCVAlue(ev)} />
        <button onClick={() => this.changeABC()}>Click Me!!!</button>

        {
          this.state.movieData.map((e) => {
            return <div className='divSupers' key={e.id}>{e.title}</div>
          })
        }
      </div>
    );
  }
}

export default App;
