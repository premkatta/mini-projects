import React, { Component } from 'react';
// import logo from './logo.svg';
import '../css/App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="leftPanel">
          <div className="menuIcon"></div>
        </div>
        <div className="divPreview">
          <div className="divSubmit">
            Submit your work
          </div>
          <div className="divBody">
            <div className="mainIcon"></div>
            <div className="labelInspiration">
              Inspiration
            </div>
            <h2 className="mainHeading">Daily inspiration, right in your face.</h2>
            <div className = "tags"> Tags:
              <span class = "blocks">
                <span className = "tag">Web Design <div class = "ico-plus" ></div></span>
                <div className = "tag" >Branding</div>
                <div className = "tag">Illustration</div>
                <div className = "tag">Animation</div>
                
              </span>
            </div>
            <div>
           </div>
          <div className = "total-cards">
            <div className = "post">
              <div className = "post-container">
              <img class = "card-img-top" src = "https://welovedaily.uk/data/1887159749168842951_3940206495.jpg"></img>
              </div>
              <div className= "post-Description">
              by <label className = "des-click">aaaa</label> 
              <label class = "des-right">1111</label>
              </div>
            </div>
            <div className = "post">
              <div className = "post-container">
              <img class = "card-img-top" src = "https://welovedaily.uk/data/1884985165602594824_3940206495.jpg"></img>
              </div>
              <div className= "post-Description">
              by <label className = "des-click">bbbb</label> 
              <label class = "des-right">2222</label>
              </div>
            </div>
            <div className = "post">
              <div className = "post-container">
              <img class = "card-img-top" src = "https://welovedaily.uk/data/1885711138471941678_3940206495.jpg"></img>
              </div>
              <div className= "post-Description">
              by <label className = "des-click">ccccc</label> 
              <label class = "des-right">333</label>
              </div>
            </div>
            <div className = "post">
              <div className = "post-container">
              <img class = "card-img-top" src = "https://welovedaily.uk/data/1884985165602594824_3940206495.jpg"></img>
              </div>
              <div className= "post-Description">
              by <label className = "des-click">dddd</label> 
              <label class = "des-right">4444</label>
              </div>
            </div>
          </div>
             
          </div>
        </div>
      </div>
    );
  }
}

export default App;
