import { combineReducers } from 'redux';
import BookData from './BookData';
import MovieData from './MovieData';


const rootReducer = combineReducers({
  state: (state = {}) => state,
  BookD: BookData,
  MovieData:MovieData
});

export default rootReducer;
