import React, { Component } from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import {getMovies} from '../actions/index';


class App extends Component {
  render() {
    return (
      // <div>React simple starter</div>
      <div>
        {this.props.books.map((e) => {
          // return <h3>{index}</h3>
          return <h2>{e.name}</h2>
        })}
        <button onClick ={()=> this.props.getMovies()}>Movies</button>
        {this.props.movies.map((e) => {
          // return <h3>{index}</h3>
          return <h2>{e.name}</h2>
        })}
      </div>
    );
  }
}
function mapStateToProps(state){
  console.log(state.BookD);
  return {books: state.BookD.data, movies:state.MovieData};
}
function mapDisapatchToProps(dispatch){
  return bindActionCreators({getMovies} ,  dispatch);
}
export default connect (mapStateToProps, mapDisapatchToProps )(App);