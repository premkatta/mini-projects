import React from 'react';
import '../css/Navbar.css';

class Navbar extends React.Component{

	constructor(props){
		super(props);
		this.state={
			name:null
		}
	}

	loginClick=()=>{
		document.getElementById('popup').style.display = 'flex';
	}

	closeButton=()=>{
		document.getElementById('popup').style.display = 'none';
	}

	submitForm=(e)=>{
		e.preventDefault();
	}
	render(){
		return(
			<div>
				<div className='nav-main'>
					<div className='nav-right'>
						<div className='login-button' onClick={this.loginClick}>
							Login
						</div>
					</div>
				</div>
				<div className='welcome'>
					{this.state.name} Welcome
				</div>
				<div id='popup' className='popup'>
					<div className='popup-content'>
						<form className='form-page' onSubmit={this.submitForm}>
							<div className='close' onClick={this.closeButton}>X</div>
							<h2>LOGIN PAGE</h2>
							<input type='text' placeholder='Enter your name' 
								onChange={(e)=>{
									this.setState({name:e.target.value});
								}}/>
							<input type='password' placeholder='Enter your password'/>
							<button>Submit</button>
						</form>
					</div>
				</div>
			</div>
		);
	}
}

export default Navbar; 