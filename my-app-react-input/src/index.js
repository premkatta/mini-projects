import React, { Component } from "react";
import ReactDOM from "react-dom";
// import "index.css" ;

class App extends Component {
    constructor (){
        super();
        this.state = {
            name : ""
        };
    }
    

    render(){
        return(
            <div>
                <h1>Hello {this.state.name} </h1>
                <input type = "text" placeholder="Type your name.." onChange={e=>this.setState({name : e.target.value})} />
            </div>
        );

    }
}

ReactDOM.render(<App />, document.getElementById("root"));